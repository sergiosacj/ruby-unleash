unleash-client(1) -- unleash feature toggle client
==================================================

## SYNOPSIS

`unleash-cilent` [`-d`] [`-h`] [`-m`] [`-q`] [`-sSLEEP`] [`-uURL`] [`-v`] [`-V`]

## DESCRIPTION

Unleash client so you can roll out your features with confidence.

## COMMAND LINE OPTIONS

  * `-d`, `--demo`:
    Demo load by looping, instead of a simple lookup

  * `-h`, `--help`:
    List all command line options

  * `-m`, `--[no-]metrics`:
    Enable metrics reporting

  * `-q`, `--quiet`:
    Quiet mode, minimum output only

  * `-sSLEEP`, `--sleep=SLEEP`:
    Sleep interval between checks (seconds) in demo

  * `-uURL`, `--url=URL`:
    URL base for the Unleash feature toggle service

  * `-v`, `--[no-]verbose`:
    Run verbosely

  * `-V`, `--variant`:
    Fetch variant for feature

## BUGS

Please report any bugs using the GitHub issue tracker:
<https://github.com/Unleash/unleash-client-ruby/issues/new>

## SEE ALSO

For more information, you may visit the source code:
<https://github.com/unleash/unleash-client-ruby>

This manual page was generated from Markdown with `ronn-ng`
<https://github.com/apjanke/ronn-ng>

## AUTHOR

Coveralls was written by Renato Arruda <rarruda@rarruda.org>,
with contributions from the community.

## DOCUMENTATION

This manual page was written by Sérgio Cipriano <sergiosacj@hotmail.com.br>.

## LICENSE

Both unleash-client and this documentation are released under the terms of the
Apache License. You may view the license on the upstream repository:
<https://github.com/Unleash/unleash-client-ruby/blob/master/LICENSE>
